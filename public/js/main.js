(function($) {
    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };
}(jQuery));


jQuery(document).ready(function($) {
	/* Nav bar menu */
	$( '#news' ).click(function(){
		$( '.dropmenu' ).slideUp();
		$( '.dropmenu' ).slideToggle();

		$( "div.navbar" ).addClass('navbar-change');
		$( this ).addClass('news-link');
		$( 'ul.navbar-nav > li.resources-center' ).removeClass('resources-center-link');
		$( 'ul.navbar-nav > li.functions' ).removeClass('functions-link');
		$( 'ul.navbar-nav > li.communities' ).removeClass('communities-link');
		$( 'ul.navbar-nav > li.initiatives' ).removeClass('initiatives-link');
		$( 'ul.navbar-nav > li.workspace' ).removeClass('workspace-link');
	});
	$('#resources-centre').click(function(){
		$( '.dropmenu' ).slideUp();
		$( '.dropmenu' ).slideToggle();

		$( "div.navbar" ).addClass('navbar-change');
		$( this ).addClass('resources-center-link');
		$( 'ul.navbar-nav > li.news' ).removeClass('news-link');
		$( 'ul.navbar-nav > li.functions' ).removeClass('functions-link');
		$( 'ul.navbar-nav > li.communities' ).removeClass('communities-link');
		$( 'ul.navbar-nav > li.initiatives' ).removeClass('initiatives-link');
		$( 'ul.navbar-nav > li.workspace' ).removeClass('workspace-link');
	});
	$('#functions').click(function(){
		$( '.dropmenu' ).slideUp();
		$( '.dropmenu' ).slideToggle();

		$( "div.navbar" ).addClass('navbar-change');
		$( this ).addClass('functions-link');
		$( 'ul.navbar-nav > li.news' ).removeClass('news-link');
		$( 'ul.navbar-nav > li.resources-center' ).removeClass('resources-center-link');
		$( 'ul.navbar-nav > li.communities' ).removeClass('communities-link');
		$( 'ul.navbar-nav > li.initiatives' ).removeClass('initiatives-link');
		$( 'ul.navbar-nav > li.workspace' ).removeClass('workspace-link');
	});
	$('#communities').click(function(){
		$( '.dropmenu' ).slideUp();
		$( '.dropmenu' ).slideToggle();

		$( "div.navbar" ).addClass('navbar-change');
		$( this ).addClass('communities-link');
		$( 'ul.navbar-nav > li.news' ).removeClass('news-link');
		$( 'ul.navbar-nav > li.resources-center' ).removeClass('resources-center-link');
		$( 'ul.navbar-nav > li.functions' ).removeClass('functions-link');
		$( 'ul.navbar-nav > li.initiatives' ).removeClass('initiatives-link');
		$( 'ul.navbar-nav > li.workspace' ).removeClass('workspace-link');
	});
	$('#initiatives').click(function(){
		$( '.dropmenu' ).slideUp();
		$( '.dropmenu' ).slideToggle();

		$( "div.navbar" ).addClass('navbar-change');
		$( this ).addClass('initiatives-link');
		$( 'ul.navbar-nav > li.news' ).removeClass('news-link');
		$( 'ul.navbar-nav > li.resources-center' ).removeClass('resources-center-link');
		$( 'ul.navbar-nav > li.functions' ).removeClass('functions-link');
		$( 'ul.navbar-nav > li.communities' ).removeClass('communities-link');
		$( 'ul.navbar-nav > li.workspace' ).removeClass('workspace-link');
	});
	$('#workspace').click(function(){
		$( '.dropmenu' ).slideUp();
		$( '.dropmenu' ).slideToggle();

		$( "div.navbar" ).addClass('navbar-change');
		$( this ).addClass('workspace-link');
		$( 'ul.navbar-nav > li.news' ).removeClass('news-link');
		$( 'ul.navbar-nav > li.resources-center' ).removeClass('resources-center-link');
		$( 'ul.navbar-nav > li.functions' ).removeClass('functions-link');
		$( 'ul.navbar-nav > li.communities' ).removeClass('communities-link');
		$( 'ul.navbar-nav > li.initiatives' ).removeClass('initiatives-link');
	});
	$( '.dropmenu' ).on('mouseleave', function(){
		$( "div.navbar" ).removeClass('navbar-change');
		$(this).slideUp();
		$( 'ul.navbar-nav > li.news' ).removeClass('news-link');
		$( 'ul.navbar-nav > li.resources-center' ).removeClass('resources-center-link');
		$( 'ul.navbar-nav > li.functions' ).removeClass('functions-link');
		$( 'ul.navbar-nav > li.communities' ).removeClass('communities-link');
		$( 'ul.navbar-nav > li.initiatives' ).removeClass('initiatives-link');
		$( 'ul.navbar-nav > li.workspace' ).removeClass('workspace-link');
	});

	function myTimer() {
		var d = new Date();
		console.log(d.toLocaleTimeString());
	}
	
	//Accordian sidebar right
	//var allPanels = $('.accordion > dd').hide();
	var allPanels = $('.accordion > dd');
	  $('.accordion > dt > a').click(function() {
		$('.accordion > dt ').css('background','url(img/add-icon.png) right 10px center  no-repeat #e2e3e4');
		$(this).parent().css('background','url(img/substract-icon.png) right 10px center  no-repeat #f8b413');
		allPanels.slideUp();
		$(this).parent().next().slideDown();
		return false;
	  });
	  
	$('#myTab a[href="#sitemap"]').click(function(){
		$('.sidebar-right').hide();
		$('.sidebar-right-mysites').show();
		$( "#profile2" ).hide();
	});
	$('#myTab a[href="#application"]').click(function(){
		$('.sidebar-right').hide();
		$('.sidebar-right-myapps').show();
		$( "#profile2" ).hide();
	});
	$('#myTab a[href="#people-directory"]').click(function(){
		$('.sidebar-right').hide();
		$('.sidebar-right-mypeople-directory').show();
	});
	
	/*Select table cell */
	$('table > tbody > tr').click(function(){
		$('table > tbody > tr').removeClass('select');
		$( this ).addClass('select');

		var tableid = $( this ).attr('data-tableid');		
		if(tableid == 1){
			$( "#profile1" ).show();
			$( "#profile2" ).hide();
		}else{
			$( "#profile2" ).show();
			$( "#profile1" ).hide();
		}
	});
	
	$( "#customize-user-apps" ).clickToggle(
		function() {
			$(this).removeClass('shortcut-apps');
			$(this).addClass('shortcut-apps-hover');
			$('.shortcut-menu2').slideUp();
			$('.shortcut-menu').slideDown();
			$( "div.navbar" ).addClass('navbar-change');
			$( "div.dropmenu" ).hide();
		},function() {
			$(this).addClass('shortcut-apps');
			$(this).removeClass('shortcut-apps-hover');
			$('.shortcut-menu').slideUp();
			$(" div.navbar").removeClass('navbar-change');
	});
	$( "#customize-user-apps" ).click(function(){
		$('#search-user-apps').addClass('shortcut-search');
		$('#search-user-apps').removeClass('shortcut-search-hover');
		
	});
	
	$( "#search-user-apps" ).click(function(){
		$('#customize-user-apps').addClass('shortcut-apps');
		$('#customize-user-apps').removeClass('shortcut-apps-hover');
	});
	
	$( "#search-user-apps" ).clickToggle(
	function() {
		$(this).removeClass('shortcut-search');
		$(this).addClass('shortcut-search-hover');
		$('.shortcut-menu').slideUp();
		$('.shortcut-menu2').slideDown();
		$( "div.navbar" ).addClass('navbar-change');
		$( "div.dropmenu" ).hide();
	},function() {
		$(this).addClass('shortcut-search');
		$(this).removeClass('shortcut-search-hover');
		$('.shortcut-menu2').slideUp();
		$(" div.navbar").removeClass('navbar-change');
		
	});
	
	// Feedback, want to do, currency button
	$('.sendfeedback-btn').click(function() {
		$('.feedback-form').slideToggle();
	});
	
	$('.wanttodo-btn').click(function() {
		$('.wanttodo-form').slideToggle();
	});
	
	$('.currency').click(function() {
		if($('.currency-drop').css('display') == 'block'){
			 var downicon = 'img/down-icon-black.png';
			$(this).css('background-image', 'url(' + downicon + ')');
		}else{
			var upicon = 'img/gt-icon-black.png';
			$(this).css('background-image', 'url(' + upicon + ')');
		}
		$('.currency-drop').slideToggle(100);
	});

	$('.remove-this').on("click", function() {
		$(this).parent().remove();
	});

	//Sitemap add your own url
	$('button.sitemap-add-url-btn').click(function() {
		var ownURL = $('input.sitemap-add-url').val();
		var lengthDiv = $('.sidebar-right-list-mysites > div > div').length;

		if(is_valid_url(ownURL) == null){
			alert('Wrong URL key in');
			return false;
		}

		$( '.sidebar-right-list-mysites > div.mysites-details-bg > div:last-child' ).after('<div class="mysites-details" style="padding:10px;height:60px;" data-sid="'+(lengthDiv+1)+'"><div style="padding:0px 0px 5px 30px;background:url(img/gt-icon.png) 5px 4px no-repeat;">'+ownURL+'</div><div class="pull-right remove-this" style="font-size:12px;height:18px;vertical-align:bottom;"><img src="img/minus-bullet.png" width="13px;"/>&nbsp;Remove</div><select class="form-control pull-right" style="font-size:12px;height:18px;width:40px;padding:1px;border-radius:0px;margin-right:5px;"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select><div class="pull-right" style="font-size:12px;margin-right:5px;vertical-align:bottom;padding-top:2px;">Position</div></div>');
		$('.sidebar-right-list-mysites > div.mysites-details-bg > div.mysites-details:last-child > select').val(lengthDiv+1);

		$('.remove-this').on("click", function() {
			$(this).parent().remove();
		});
	});

	//Validate url
	function is_valid_url(url)
	{
	     return url.match(/^[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
	}

	$('.sitemap-ulist > li').click(function() {
		var thisURL = $(this).text();
		var lengthDiv = $('.sidebar-right-list-mysites > div > div').length;
		$( '.sidebar-right-list-mysites > div.mysites-details-bg > div:last-child' ).after('<div class="mysites-details" style="padding:10px;height:60px;" data-sid="'+(lengthDiv+1)+'"><div style="padding:0px 0px 5px 30px;background:url(img/gt-icon.png) 5px 4px no-repeat;">'+thisURL+'</div><div class="pull-right remove-this" style="font-size:12px;height:18px;vertical-align:bottom;"><img src="img/minus-bullet.png" width="13px;"/>&nbsp;Remove</div><select class="form-control pull-right" style="font-size:12px;height:18px;width:40px;padding:1px;border-radius:0px;margin-right:5px;"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option></select><div class="pull-right" style="font-size:12px;margin-right:5px;vertical-align:bottom;padding-top:2px;">Position</div></div>');
		$('.sidebar-right-list-mysites > div.mysites-details-bg > div.mysites-details:last-child > select').val(lengthDiv+1);
		$('.remove-this').on("click", function() {
			$(this).parent().remove();
		});
	});

	$('.sidebar-right-list-mysites > div.mysites-details-bg > div.mysites-details > select').on('change', function() {		
		dataid = $( this ).val();
		
		$( this ).parent().attr('data-sid', dataid);

		$('.sidebar-right-list-mysites > div.mysites-details-bg > div.mysites-details').sort(function(a,b){
   			return a.getAttribute('data-sid') > b.getAttribute('data-sid')
		}).appendTo('.sidebar-right-list-mysites > div.mysites-details-bg');
	});

	//Application manage icon
	$('.icon-details > span.add-icon').click(function() {
		var imgIcon = $(this).parent().parent().children('img').attr('src');
		var titleIcon = $(this).prev().text();
		var lengthDiv = $( '.myapps-icon > div' ).length;
		if(lengthDiv < 9){
			$( '.myapps-icon > div:last-child' ).after('<div class="col-xs-4 col-md-4 text-center" data-sid="'+(lengthDiv+1)+'"><img src="'+imgIcon+'" /><span>'+titleIcon+'</span><div class="clearfix"></div><div class="pull-left" style="font-size:12px;margin-right:5px;vertical-align:bottom;padding-top:2px;">Position</div><select class="form-control pull-left"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select><div class="clearfix"></div><div class="remove-this"><img src="img/minus-bullet.png" width="13px;"/>&nbsp;Remove</div></div>');
			$( '.myapps-icon > div:last-child > select' ).val(lengthDiv+1);
		}else{
			alert('Enough for icon to add');
		}
		$('.remove-this').on("click", function() {
			$(this).parent().remove();
		});
	});

	//Application add apps
	$('.myapps-icon > div').sort(function(a,b){
   		return a.dataset.sid > b.dataset.sid
	}).appendTo('.myapps-icon');

	$('.myapps-icon > div > select').on('change', function() {		
		dataid = $( this ).val();
		
		$( this ).parent().attr('data-sid', dataid);

		$('.myapps-icon > div').sort(function(a,b){
   			return a.getAttribute('data-sid') > b.getAttribute('data-sid')
		}).appendTo('.myapps-icon');
	});

	
	// Javascript to enable link to tab
	var url = document.location.toString();
	console.log(url.split('#')[1]);
	if (url.match('#')) {
		$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
		if(url.split('#')[1] == 'sitemap'){
			$('.sidebar-right').hide();
			$('.sidebar-right-mysites').show();
		}
		if(url.split('#')[1] == 'application'){
			$('.sidebar-right').hide();
			$('.sidebar-right-myapps').show();
		}
		if(url.split('#')[1] == 'people-directory'){
			$('.sidebar-right').hide();
			$('.sidebar-right-mypeople-directory').show();
		}
	} 

	// Change hash for page-reload
	$('.nav-tabs a').on('shown', function (e) {
		window.location.hash = e.target.hash;
	});

	var tid = setInterval(dateTime, 500);

	function dateTime(){
		var dy = ['MONDAY','TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
		var mn=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];

		var date = new Date();
		var td = date.getDay()
		var tday = dy[td-1];

		var d  = date.getDate();
		var day = (d < 10) ? '0' + d : d;
		var m = date.getMonth() + 1;
		var month = mn[m-1];
		var yy = date.getYear();
		var year = (yy < 1000) ? yy + 1900 : yy;

		var h = date.getHours();
		var hours;
		var am_pm;
		if(h < 12){
			 hours = h;
			 am_pm = 'am';
		}else{
			hours = h - 12;
			am_pm = 'pm';
		}
		var mt = date.getMinutes();
		var minutes = (mt < 10) ? '0' + mt : mt;

		$(' #date-time' ).html(tday + ", " + day + " " + month + " " + year + ", " + hours + ":" + minutes + am_pm);
	}

	var todaydate=new Date();
	var curmonth=todaydate.getMonth()+1; //get current month (1-12)
	var curyear=todaydate.getFullYear(); //get current year

	var calendarstr=buildCal(curmonth, curyear, "main", "month", "daysofweek", "days", 0);
	$( '#calendarspace' ).html(calendarstr);

	function updateCalender(curmonth, curyear){
		var calendarstr=buildCal(curmonth, curyear, "main", "month", "daysofweek", "days", 0);
		$( '#calendarspace' ).html(calendarstr);
	}

});