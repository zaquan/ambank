module.exports = function(grunt)	{
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		config: grunt.file.readJSON('config.json'),
		concat: {
    		options: {
    			separator: ';'
      		},
      		dist: {
        		//src: ['public/js/*.js'],
        		src: ['server.js'],
        		dest: 'public/js/<%= pkg.name %>.js'
      		}
    	},
	    uglify: {
	      options: {
	        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
	      },
	      dist: {
	        files: {
	          'public/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
	        }
	      }
	    },
	    imagemin: {
	    	dynamic: {
		    	files: [{
		    		expand: true,
		    		cwd: 'public/',
		    		src: ['**/*.{png,jpg,gif}'],
		    		dest: 'public/'
		    	}]
	    	}
	    }, 
	    ftpush: {
	    	build: {
	    		auth: {
	    			host: '<%= config.ftp.host %>',
	    			port: '<%= config.ftp.port %>',
	    			authKey: '<%= config.ftp.authKey %>'
	    		},
	    		src: '<%= config.ftp.src %>',
	    		dest: '<%= config.ftp.dest %>',
	    		exclusions: ['public/js/.DS_Store', 'public/js/Thumbs.db', 'dist/tmp'],
	    		keep: ['public/js/*.jpg']
	    	}
	    },
		jshint: {
			files: ['Gruntfile.js', '*.js'],
			options: {
				// options here to override JSHint defaults
				globals: {
					jQuery: true,
					console: true,
					module: true,
					document: true
				}
			}
		},
		watch: {
			files: ['<%=jshint.files %>'],
			task: ['jshint']
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
  	grunt.loadNpmTasks('grunt-contrib-jshint');
  	grunt.loadNpmTasks('grunt-contrib-watch');
  	grunt.loadNpmTasks('grunt-contrib-concat');
  	grunt.loadNpmTasks('grunt-ftpush');
  	grunt.loadNpmTasks('grunt-mysql-dump-import');
  	grunt.loadNpmTasks('grunt-peach');
  	grunt.loadNpmTasks('grunt-contrib-imagemin');

	grunt.registerTask('default', ['jshint', 'uglify', 'imagemin', 'ftpush']);
};